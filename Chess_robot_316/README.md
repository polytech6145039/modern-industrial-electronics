## Описание
Описание содержится в [GitHub репозитории](https://github.com/jfunfor/chess_robot)
[Доска проекта](https://app.striveapp.ru/spaces/44032/93693/tasks)

## Ролевая модель

| Функция                           | Количество | Навыки                                                          |
|-----------------------------------|------------|-----------------------------------------------------------------|
| Веб-разработчик                   | 1          | any frontend framework, html, css, docker, linux                |
| Back-end разработчик              | 2          | any back-end framework, websocket, tcp, docker, linux, git      |
| Разработчик мобильных приложений  | 1          | flutter, websocket, git                                         |




## Команда

|№| ФИО                         |Группа       |
|-|-----------------------------|-------------|
|1| Го Цзядун                   |5132704/20701|
|2| Афонин Арсений Сергеевич    |5132704/20502|
|3| Петров Илья Андреевич       |5132704/20502|
|4| Кудрин Данил Дмитриевич     |5132704/20502|

